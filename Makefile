dev_deploy:
	zappa deploy dev

dev_undeploy:
	zappa undeploy --remove-logs

dev_server:
	FLASK_APP=blog.py flask run
